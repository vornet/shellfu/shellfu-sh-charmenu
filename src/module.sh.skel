#!/bin/sh
#shellcheck disable=SC3043


CHARMENU__FILE="${CHARMENU__FILE:-}"


charmenu() {
    #
    # Present "1-char-menu" (but actually N-char) or its parts
    #
    # Usage:
    #     CHARMENU__FILE=/path/to/file
    #     charmenu [options] full [ARG]...
    #     charmenu [options] items
    #     charmenu [options] prompt
    #     charmenu [options] has ITEM
    #
    # Read file in $CHARMENU__FILE and display either `full`
    # menu, `items` menu (e.g. "y,n,q,?") or just `prompt`.
    #
    # Options:
    #
    #     -f FILE, --file FILE   Read contents of menu freom FILE
    #         instead of variable `$CHARMENU__FILE`.
    #
    #     -p PROMPT, --prompt PROMPT   Set prompt string to PROMPT.
    #         If PROMPT contains '%s', it will be replaced by 'items'
    #         menu (eg. "y,n,q").  Note that you most probably want
    #         to include space as last character.  Default: "What
    #         next [%s]? ".
    #
    #     -i, --ignore-case   Ignore case of user reply.  Default:
    #         case is respected.
    #
    #     -d DLM, --delimiter DLM   Use delimiter DLM when printing
    #         'items' menu.  Default: "," (comma).
    #
    # Format of menu file is one item per line, where item is
    # delimited from its description by single colon. Leading
    # and trailing spaces and spaces around the colon are
    # stripped.
    #
    # Simple example menu file:
    #
    #     a: abort
    #     r: retry
    #     f: fail
    #
    # You can use printf symbols like `%s` in menu and then
    # call `charmenu full arg1 arg2...` to have your arguments
    # expanded in the full menu.  More fleshed out example:
    #
    #     . "$(sfpath)" || exit 3
    #     shellfu import charmenu
    #     shellfu import pretty
    #
    #     cat <<EOM >menu
    #     r: re-run test %s
    #     c: clean screen
    #     d: download results to %s
    #     q: quit
    #     EOM
    #
    #     CHARMENU__FILE=menu
    #     running=true
    #     while $running; do
    #         charmenu full "mytest" "some storage"
    #         read -r response
    #         case $response in
    #             r)  rerun_test ;;
    #             c)  reset ;;
    #             d)  download_results ;;
    #             q)  running=false ;;
    #             *)  warn "invalid response: $response"
    #         esac
    #     done
    #     clean_up
    #
    # `charmenu has ITEM` exits with zero if ITEM is a valid
    # menu item.  With abort/retry/fail example above, calling
    # `if charmenu has a; echo OK; fi` would print "OK"..
    #
    local __charmenu__mfile     # path to menu file
    local __charmenu__prompt    # prompt text
    local __charmenu__delim     # delimiter (in 'items' menu)
    local __charmenu__cmd       # command do execute
    local __charmenu__cases     # "-i" in case-insensitive mode
    local __charmenu__menu      # loaded menu text
    local __charmenu__alist     # loaded list of items
    __charmenu__mfile=$CHARMENU__FILE
    __charmenu__prompt="What next [%s]? "
    __charmenu__delim=,
    while true; do case $1 in
        -d|--delimiter)         __charmenu__delim="$2";     shift 2 || return 2 ;;
        -f|--file)              __charmenu__mfile="$2";     shift 2 || return 2 ;;
        -i|--ignore-case)       __charmenu__cases="-i";     shift               ;;
        -p|--prompt)            __charmenu__prompt="$2";    shift 2 || return 2 ;;
        full|items|prompt|has)  __charmenu__cmd=$1;         shift; break        ;;
        *)  mkusage "full [PRINTF_ARG...]" \
                    "items [DELIM]"        \
                    "prompt [PRINTF_FMT]"  \
                    "has ITEM"                                      ;;
    esac done
    __charmenu__menu="$(cat "$__charmenu__mfile")"
    __charmenu__alist="$(echo "$__charmenu__menu" | cut -d: -f1 | sed -re 's/^ +//; s/ +$//')"
    #shellcheck disable=SC2059
    case $__charmenu__cmd in
        full)   # print the full menu, with any printf notation expanded
            printf "$__charmenu__menu\n" "$@"
            ;;
        items)  # print just the list of actions
            echo "$__charmenu__alist" paste -sd"$__charmenu__delim"
            ;;
        prompt) # print prompt for user
            printf "$__charmenu__prompt" "$(charmenu items)"
            ;;
        has)    # check if response is valid ("on the menu")
            local __charmenu__res="$1"
            test -n "$__charmenu__res" || return 1      # "" is not valid
            echo "$__charmenu__alist" | grep $__charmenu__cases -e "^$__charmenu__res$"
            return $?
            ;;
    esac
}

#shellfu module-version=__MKIT_PROJ_VERSION__
